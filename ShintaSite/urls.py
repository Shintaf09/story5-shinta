from .views import *
from django.urls import path
urlpatterns = [
    path('', index, name="home"),
    path('home/', index, name="home"),
    path('about/', about, name="about"),
    path('resume/', resume, name="resume"),
    path('contact/', contact, name="contact"),
    path('gallery/', gallery, name="gallery"),
    path('schedule/', schedule, name="schedule"),
    path('addschedule/', addSchedule, name="addschedule"),
    path('delete/', delete, name="delete"),
    
]
