from django import forms

class AktivitasShintaForm(forms.Form):
 	waktu_mulai = forms.DateTimeField (input_formats=['%Y-%m-%dT%H:%M'])
 	waktu_selesai = forms.DateTimeField (input_formats=['%Y-%m-%dT%H:%M'])
 	pesan = forms.CharField (max_length=100)
 	nama_kegiatan = forms.CharField (max_length=100)
 	tempat = forms.CharField (max_length=100)
 	kategori = forms.CharField (max_length=100)