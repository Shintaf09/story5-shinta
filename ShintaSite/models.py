from django.db import models

class AktivitasShinta(models.Model):
 	waktu_mulai = models.DateTimeField ('Start Time')
 	waktu_selesai = models.DateTimeField ('End Time')
 	pesan = models.CharField (max_length=100)
 	nama_kegiatan = models.CharField (max_length=100, default = '')
 	tempat = models.CharField (max_length=100, default = '')
 	kategori = models.CharField (max_length=100, default = '')
 	