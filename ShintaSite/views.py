from django.shortcuts import render, redirect
from .models import AktivitasShinta
from .forms import AktivitasShintaForm

# Create your views here.
def index(request):
	return render(request,"index.html")

def about(request):
	return render(request,"aboutme.html")

def resume(request):
	return render(request,"resume.html")

def contact(request):
	return render(request,"contact.html")

def gallery(request):
	return render(request,"gallery.html")

def schedule(request):
	list_aktivitasku = AktivitasShinta.objects.all().values()
	return render(request,"schedule.html", {'Aktivitas_Shinta' : list_aktivitasku})

def delete(request):
	if request.method == 'POST'  :
		id = request.POST['id']
		AktivitasShinta.objects.filter(id=id).delete()
	return redirect('/schedule')

def addSchedule (request):
	print("Meong")
	if request.method == 'POST' : 
		print("Zzzzz")
		form = AktivitasShintaForm(request.POST)
		if form.is_valid():
			print("Alghi")
			waktu_mulai = request.POST ['waktu_mulai']
			waktu_selesai = request.POST['waktu_selesai']
			pesan = request.POST ['pesan']
			nama_kegiatan = request.POST ['nama_kegiatan']
			tempat = request.POST ['tempat']
			kategori = request.POST ['kategori']
			aktivitasku = AktivitasShinta(waktu_mulai=waktu_mulai, waktu_selesai=waktu_selesai, pesan=pesan, nama_kegiatan=nama_kegiatan, tempat=tempat, kategori=kategori)
			aktivitasku.save()
	return redirect('/schedule')

